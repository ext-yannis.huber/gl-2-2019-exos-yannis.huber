package ex_pattern_observer;

class ObserverObservableDemo {
    
    public static void main(String args[]) { 
        
        // TODO
        // 1) cr�er un objet � observer :
        YoutubeChannel yc = new YoutubeChannel();
        
        // 2) cr�er un observateurs "admin"
        AdminObserver ao = new AdminObserver();
        
        // 3) ajouter "admin" � la liste d'observateurs de l'objet :
        yc.addObserver(ao);
        
        // 4) lancer les notifications (les changements avec les news) :
        yc.news();
        // 5) r�p�ter les actions 2) et 3) pour un "first follower" et un 
        // "second follower"
        FirstFollower ff = new FirstFollower();
        SecondFollower sf = new SecondFollower();
        yc.addObserver(ff);
        yc.addObserver(sf);
        yc.news();
        // QUESTION avant le point 6) : Dans quel ordre sont notifi�s les 
        // observateurs selon vous ?
        // Aucune idée
        
        // 6) relancer les notifications et controler votre r�ponse � la 
        // question qui pr�c�de : ordre inverse à l'ajout
        
    }
}
