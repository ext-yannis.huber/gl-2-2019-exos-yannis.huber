package ex_pattern_observer;

import java.util.Observable;
import java.util.Observer;

public class FirstFollower implements Observer {

    @Override
    public void update(Observable arg0, Object arg1) {
        System.out.println("Follower 1 got the news:"+(String)arg1);
    }

    //TODO
    // 1) ajouter la m�thode n�cessaire � l'impl�mentation :
    // N.B. Pour l'exercice vous pouvez mettre comme corps de la m�thode quelque
    // chose comme : System.out.println("Follower 1 got the news:"+(String)arg);
    
}