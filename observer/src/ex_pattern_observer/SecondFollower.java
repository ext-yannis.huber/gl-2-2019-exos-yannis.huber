package ex_pattern_observer;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

public class SecondFollower implements Observer {
    
    static ArrayList<String> notificationsList = new ArrayList<String>();

    @Override
    public void update(Observable arg0, Object arg1) {
        notificationsList.add((String) arg1);
        for(String s : notificationsList){
            System.out.println(s);
        }
    }
    
    //TODO
    // 1) ajouter la m�thode n�cessaire � l'impl�mentation :
    // N.B. Pour le corps de la m�thode, il faut ajouter la nouvelle notification
    // � la "notificationsList" puis afficher tous les �l�ments de la liste.
    
}